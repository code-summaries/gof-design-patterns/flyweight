<div align="center">
  <h1>Flyweight</h1>
</div>

<div align="center">
  <img src="flyweight.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Flyweight is a structural pattern to reduce the required storage space for objects by sharing
common parts of state.**

### Real-World Analogy

_Furniture placeholders._

It's hard work to move real furniture around when experimenting with different arrangements (contexts). Instead, you
might use lightweight placeholders (flyweight) to represent the different pieces of furniture.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

Flyweight (Glyph)- declares an interface through which flyweights can receive and act on extrinsic state.FLYWEIGHT 199 •
ConcreteFlyweight (Character)- implements the Flyweight interface and adds storage for intrinsic state, if any. A
ConcreteFlyweight object must be sharable. Any state it stores must be intrinsic; that is, it must be independent of the
ConcreteFlyweight object's context. • UnsharedConcreteFlyweight (Row, Column)- not all Flyweight subclasses need to be
shared. The Flyweight interface enables sharing; it doesn't enforce it. It's common for UnsharedConcreteFlyweight
objects to have ConcreteFlyweight objects as children at some level in the flyweight object structure (as the Row and
Column classes have). • FlyweightFactory- creates and manages flyweight objects.- ensures that flyweights are shared
properly. When a client requests a flyweight, the FlyweightFactory object supplies an existing instance or creates one,
if none exists. • Client- maintains a reference to flyweight(s).- computes or stores the extrinsic state of flyweight(s

### Collaborations

...
State that a flyweight needs tofunction must be characterized aseither intrinsic
or extrinsic. Intrinsic state is stored in the ConcreteFlyweight object; extrinsic
state is stored or computed by Client objects. Clients pass this state to the
flyweight when they invoke its operations.
• Clients should notinstantiate ConcreteFlyweights directly.Clients must obtain
ConcreteFlyweight objects exclusively from the FlyweightFactory object to
ensure they are shared properly.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

The Flyweight pattern's effectiveness depends heavily on how and where it's
used. Apply the Flyweight pattern when allof the followingare true:
• An application uses a large number of objects.
• Storage costs are high because ofthe sheer quantity ofobjects.
• Most object state canbe made extrinsic.
• Many groups of objects may be replaced by relatively few shared objects
once extrinsic state isremoved.
• The application doesn't depend on object identity. Since flyweight objects
may be shared, identity tests will return true for conceptually distinct objects.

### Motivation

- ...

Some applications could benefit from using objects throughout their design, but
a naive implementation would be prohibitively expensive.

For example, most document editor implementations have text formatting and
editing facilities that are modularized to some extent.Object-oriented document
editors typically use objects to represent embedded elements like tables and figures. However, they usually stop short
of using an object for each characterin the
document, even though doing so would promote flexibility at the finest levels in
the application. Characters and embedded elements could then be treated uniformly with respect to how they are drawn
and formatted. The application could
be extended to support new charactersets without disturbing other functionality.
The application's object structure could mimic the document's physical structure.
The followingdiagram shows how a document editor can use objects to represent
characters.

The drawback of such a design is its cost. Even moderate-sized documents may
require hundreds of thousands of character objects, which will consume lots of
memory and may incur unacceptable run-time overhead. The Flyweight pattern
describes how to share objects to allow their use at fine granularities without
prohibitive cost.

A flyweightis a shared object that can be used in multiple contexts simultaneously.
The flyweight acts as an independent object in each context—it's indistinguishable from an instance of the object that'
s not shared. Flyweights cannot make
assumptions about the context in which they operate. The key concept here is the
distinction between intrinsic and extrinsic state. Intrinsic state is stored in the
flyweight; it consists of information that'sindependent of the flyweight's context,
thereby making it sharable. Extrinsic state depends on and varies with the flyweight's context and therefore can't be
shared. Client objects are responsible for
passing extrinsic state to the flyweight when it needs it.

Flyweights model concepts or entities that are normally too plentiful to represent
with objects.For example, a document editor can create a flyweight for each letter
of the alphabet. Each flyweight stores a character code, but its coordinate position
in the document and its typographic style can be determined from the text layout
algorithms and formatting commands in effect wherever the character appears.
The character code is intrinsic state, while the other information is extrinsic.

Logically there is an object for every occurrence of a given character in the document:

Physically, however, there is one shared flyweight object per character, and it
appears in different contextsin the document structure. Each occurrence of a particular character object refers to the
same instance in the shared pool of flyweight
objects:

The class structure for these objectsis shown next. Glyph is the abstract classfor
graphical objects, some ofwhich may be flyweights.Operations that may depend
on extrinsic state have it passed to them as a parameter. For example, Draw and
Intersects must know which context the glyph is in before they can do their job.

A flyweight representing the letter "a" only stores the corresponding character
code; it doesn't need to store its location or font. Clients supply the contextdependent information that the flyweight
needs to draw itself. For example, a
Row glyph knows where its children should draw themselves so that they are
tiled horizontally.Thus it can pass each child its location in the draw request.

Because the number of different character objectsis far less than the number of
characters in the document, the total number of objectsis substantially less than
what a naive implementation would use. A document in which all characters appear in the same font and color will
allocate on the order of 100characterobjects
(roughly the size of the ASCII characterset) regardless of the document's length.
And since most documents use no more than 10 different font-color combinations, this number won't grow appreciably in
practice.An object abstraction thus
becomes practical for individual characters.

---

Use the Flyweight pattern only when your program must support a huge number of objects which barely fit into available
RAM.

The benefit of applying the pattern depends heavily on how and where it’s used. It’s most useful when:

an application needs to spawn a huge number of similar objects
this drains all available RAM on a target device
the objects contain duplicate states which can be extracted and shared between multiple object

### Known Uses

Text Processing: In text editors or word processors, the Flyweight pattern can be used to handle character formatting
attributes (such as font, size, color) to optimize memory usage for text with similar attributes.

Graphics and GUIs: When dealing with graphical objects like icons, glyphs, or shapes that share common properties (like
color, texture), the Flyweight pattern can be applied to avoid redundant memory consumption.

Caching: Flyweight is often used in caching mechanisms to reuse existing objects instead of creating new ones, improving
performance by avoiding unnecessary object creation.

Game Development: In gaming, especially for scenarios where a large number of similar objects exist (e.g., bullets,
particles, textures), the Flyweight pattern can optimize memory by sharing common properties across these objects.

Databases and Data Structures: When managing large datasets with repeated elements, the Flyweight pattern can be used to
efficiently represent and handle similar data.

Network Protocol Implementations: In cases where network protocols involve a lot of repetitive data structures, the
Flyweight pattern can help in reducing memory overhead during communication.

Performance-Critical Applications: Any application dealing with a massive number of objects that can be shared or reused
without impacting the application's logic can benefit from the Flyweight pattern to enhance performance.

Document Management Systems: In applications dealing with documents (text documents, spreadsheets, presentations), where
formatting attributes like fonts, styles, or colors can be shared among multiple sections or elements within the
document.

Web Development: Caching commonly used resources like CSS styles, images, or templates across multiple web pages to
reduce server load and improve page load times.

Simulation and Modeling: In simulations or modeling applications where multiple instances of similar objects (such as
particles, terrain tiles, or environmental elements) need to be represented efficiently.

Localization and Internationalization: When managing translations or different versions of content where some parts
remain constant across languages or versions, the Flyweight pattern can be applied to handle shared content efficiently.

Financial Applications: In financial systems handling transactions, where certain attributes or metadata of
transactions (like currencies, transaction types) can be shared among multiple entries.

Data Compression: Flyweight pattern can be used in compression algorithms where repeated patterns or symbols are stored
once and referred to multiple times to reduce storage requirements.

Resource Pooling: Managing connections or resources in pools (like database connections, thread pools) where similar
resources can be reused instead of creating new ones.

Chat or Messaging Systems: Storing shared information (like user profiles, emojis, or common message templates)
efficiently in memory to reduce the overhead of repeated data across conversations.

java.lang.Integer#valueOf(int) (also on Boolean, Byte, Character, Short, Long and BigDecimal)

The concept of flyweight objects was first described and explored as a design
technique in Interviews 3.0 [CL90]. Its developers built a powerful document
editor called Doc as a proof of concept [CL92]. Doc uses glyph objects to repre-
sent each character in the document. The editor builds one Glyph instance for
each character in a particular style (which defines its graphical attributes); hence
a character's intrinsic state consists of the character code and its style informa-
tion (an index into a style table).4 That means only position is extrinsic, making
Doc fast. Documents are represented by a class Document, which also acts as the
FlyweightFactory. Measurements on Doc have shown that sharing flyweight char-
acters is quite effective. In a typical case, a document containing 180,000 characters
required allocation of only 480 character objects.
ET++ [WGM88] uses flyweights to support look-and-feel independence.5 The
look-and-feel standard affects the layout of user interface elements (e.g., scroll
bars, buttons, menus—known collectively as "widgets") and their decorations
(e.g., shadows, beveling). A widget delegates all its layout and drawing behavior
to a separate Layout object. Changing the Layout object changes the look and feel,
even at run-time.
For each widget class there is a corresponding Layout class (e.g., ScrollbarLayout,
MenubarLayout, etc.). An obvious problem with this approach is that using sep-
arate layout objects doubles the number of user interface objects: For each user
interface object there is an additional Layout object. To avoid this overhead, Lay-
out objects are implemented as flyweights. They make good flyweights because
they deal mostly with defining behavior, and it's easy to pass them what little
extrinsic state they need to lay out or draw an object.
The Layout objects are created and managed by Look objects. The Look class is
an Abstract Factory (87) that retrieves a specific Layout object with operations
like GetButtonLayout, GetMenuBarLayout, and so forth. For each look-and-feel
standard there is a corresponding Look subclass (e.g., MotifLook, OpenLook) that
supplies the appropriate Layout objects.
By the way, Layout objects are essentially strategies (see Strategy (315)). They are
an example of a strategy object implemented as a flyweight.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Composition**

Structural patterns are concerned with how classes and objects are composed to form larger structures.
Structural class patterns use inheritance to compose interfaces or implementations.
Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- Storage costs of objects.

### Solution to causes of redesign

None (known)

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

Flyweights may introduce run-time costs associated with transferring, finding,
and/or computing extrinsic state, especially if it was formerly stored as intrinsic
state. However, such costs are offset by space savings, which increase as more
flyweights are shared.
Storage savings are a function of several factors:
• the reduction in the total number of instances that comes from sharing
• the amount of intrinsic state per object
• whether extrinsic state is computed or stored.
The more flyweights are shared, the greater the storage savings. The savings
increase with the amount of shared state. The greatest savings occur when the
objects use substantial quantities of both intrinsic and extrinsic state, and the
extrinsic state can be computed rather than stored. Then you save on storage in
two ways: Sharing reduces the cost ofintrinsic state, and you trade extrinsic state
for computation time.
The Flyweight pattern is often combined with the Composite (163) pattern to
represent a hierarchicalstructure as a graph with shared leaf nodes. A consequence
of sharing isthat flyweightleaf nodes cannot store a pointerto their parent.Rather,
the parent pointer is passed to the flyweight as part of its extrinsic state. This has
a major impact on how the objects in the hierarchy communicate with each other.

You can save lots of RAM, assuming your program has tons of similar objects.

You might be trading RAM over CPU cycles when some of the context data needs to be recalculated each time somebody calls
a flyweight method.
The code becomes much more complicated. New team members will always be wondering why the state of an entity was
separated in such a way.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

The Flyweight pattern is often combined with the Composite (163) pattern to
implement a logically hierarchical structure in terms of a directed-acyclic graph
with shared leaf nodes.
It's often best to implement State (305) and Strategy (315) objects as flyweights.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

Flyweight can be implemented by a creation method that returns cached objects instead of creating new.

(recognizable by creational methods returning a cached instance, a bit the "multiton" idea)

### Structure

```mermaid
classDiagram
    class FlyweightFactory {
        - flyweights: Map<string, Flyweight>
        + getFlyweight(key: string): Flyweight
    }

    class Flyweight {
        <<interface>>
        + operation(extrinsicState: any): void
    }

    class ConcreteFlyweight {
        - intrinsicState: any
        + operation(extrinsicState: any): void
    }

    Flyweight <|.. ConcreteFlyweight: implements
    FlyweightFactory o-- Flyweight: aggregated by
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

Consider the following issues when implementing the Flyweight pattern:

1. Removing extrinsic state. The pattern's applicability is determined largely by
   how easy it is to identify extrinsic state and remove it from shared objects.
   Removing extrinsic state won't help reduce storage costs if there are as many
   different kinds of extrinsic state as there are objects before sharing. Ideally,
   extrinsic state can be computed from a separate object structure, one with far
   smaller storage requirements.
   In our document editor, for example, we can store a map of typographic
   information in a separate structure rather than store the font and type style
   with each character object. The map keeps track of runs of characters with
   the same typographic attributes. When a character draws itself, it receives
   its typographic attributes as a side-effect of the draw traversal. Because doc-
   uments normally use just a few different fonts and styles, storing this infor-
   mation externally to each character object is far more efficient than storing it
   internally.
2. Managing shared objects. Because objects are shared, clients shouldn't instanti-
   ate them directly. FlyweightFactory lets clients locate a particular flyweight.
   FlyweightFactory objects often use an associative store to let clients look up
   flyweights of interest. For example, the flyweight factory in the document
   editor example can keep a table of flyweights indexed by character codes. The
   manager returns the proper flyweight given its code, creating the flyweight
   if it does not already exist.
   Sharability also implies some form of reference counting or garbage collection
   to reclaim a flyweight's storage when it's no longer needed. However, neither
   is necessary if the number of flyweights is fixed and small (e.g., flyweights
   for the ASCII character set). In that case, the flyweights are worth keeping
   around permanently.

### Implementation

In the example we apply the flyweight pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Flyweight](https://refactoring.guru/design-patterns/flyweight)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Geekific: The Flyweight Pattern Explained and Implemented in Java](https://youtu.be/qscOsQV-K14?si=A0dlf-HdUB-nlD4w))
- [Jack Herington: Proxy & Flyweight Patterns - No BS TS Series 2 Episode 5](https://youtu.be/0vumsisnqwM?si=e8E9wkgFGdq6W2Ti)

<br>
<br>
